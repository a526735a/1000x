module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb-typescript',
    'prettier',
    'prettier/react',
    'prettier/@typescript-eslint',
  ],
  plugins: ['@typescript-eslint', 'prettier'],
  parserOptions: {
    project: './tsconfig.json',
    files: ['*.ts', '*.tsx'],
  },
  rules: {
    'prettier/prettier': 'error',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        ts: 'never',
        tsx: 'never',
      },
    ],
    'import/prefer-default-export': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'react/prop-types': 'off',
    'react/jsx-filename-extension': ['error', { extensions: ['.tsx'] }],
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': 'error',
    'jsx-a11y/anchor-is-valid': 'off',
    'react/jsx-props-no-spreading': 0,
    'react/require-default-props': 0,
    'jsx-a11y/label-has-associated-control': [
      'error',
      {
        controlComponents: ['Select', 'Input'],
      },
    ],
  },
  settings: {
    'import/resolver': {
      typescript: {
        project: './',
      },
    },
  },
  overrides: [
    {
      files: ['./**/*.test.ts', './**/*.test.tsx'],
      globals: {
        jest: true,
      },
    },
  ],
};
