import addresses from '~/constants/contracts';

export const getAddress = (address: any): string => {
  const chainId = process.env.CHAIN_ID ?? '4';
  const mainnetChainId = process.env.CHAIN_ID ?? '56';
  return address[chainId] ? address[chainId] : address[mainnetChainId];
};

export const getInsuranceContractAddress = () => {
  return getAddress(addresses.insurance);
};
