module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {    
      colors: {
      primary:'#0D3693',
      secondary:'#F8F9FE',
      grey:"#929497",
      darkblue:"#00AEC5",
      darkpurple:"#4C2D83",
      neon:"#EDE945"
    }
  },
  },
  variants: {
    extend: {},
  },
  plugins: []
}
