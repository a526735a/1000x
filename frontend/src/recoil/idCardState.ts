import { atom } from 'recoil';

export const idCardState = atom({
  key: 'idCardState',
  default: '',
});
