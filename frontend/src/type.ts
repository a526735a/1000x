export interface Insurance {
  name: string;
  id: string;
  info: string;
  policy: string;
}