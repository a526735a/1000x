import { RefObject, FC } from 'react';


interface CreatePassWordProps {
  newPasswordRef: RefObject<HTMLInputElement>;
  confirmPasswordRef: RefObject<HTMLInputElement>;
  handleConfirm: () => void;
}

const CreatePassWord: FC<CreatePassWordProps> = ({ newPasswordRef, confirmPasswordRef, handleConfirm }) => (
  <div className="flex flex-col gap-9">
    <p className="text-center text-primary text-5xl font-bold">
      <span className="text-darkpurple">BCS</span>
      <span className="text-darkpurple text-7xl">(</span>
      <span className="text-grey">I</span>
      <span className="text-darkblue">0</span>
      <span className="text-darkblue">0</span>
      <span className="text-darkblue">0</span>
      <span className="text-neon">X</span>
    </p>
    <div className="flex flex-col gap-4">
      <div>
        <p className="text-grey text-sm mb-2">
          New password (min 8 character)
        </p>
        <input className="border w-full p-1 rounded-md shadow-md" ref={newPasswordRef} type="password" />
      </div>
      <div>
        <p className="text-grey text-sm mb-2">Confirm Password</p>
        <input className="border w-full p-1 rounded-md shadow-md" ref={confirmPasswordRef} type="password" />
      </div>
    </div>
    <button
      className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
      onClick={handleConfirm}
    >
      Create
    </button>
  </div>
)

export default CreatePassWord;
