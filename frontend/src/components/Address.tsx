import React from 'react'
import Image from "next/image";

interface AddressProps {
  address?: string;
  addressIndex?: number;
}

const Address: React.FC<AddressProps> = ({ address, addressIndex }) => {
  return (
    <div className="flex flex-row items-center">
      <div className="flex-row">
        {address ? (
          <><p className="text-gray-500 text-xs font-bold">{`Account ${addressIndex}`}</p><p className="text-gray-500 text-xs font-bold">{`${address.slice(0, 6)}...${address.slice(address.length - 4)}`}</p></>
        ) : <p className="text-gray-500 text-xs font-bold">{`Please Login First`}</p>
      }
      </div>
      <div className="flex items-center justify-center w-16 h-16 rounded-full">
        <Image
          className="rounded-full"
          src="https://picsum.photos/200"
          alt="Picture of the author"
          width={28}
          height={28} />
      </div>
    </div>
  )
}

export default Address
