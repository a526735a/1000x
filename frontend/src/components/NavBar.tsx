import React, { useEffect } from 'react';
import Address from './Address';
import { walletState } from '~/recoil/walletState';
import { useRecoilValue } from 'recoil';
import { useRouter } from 'next/router';

const NavBar: React.FC = () => {
  const wallet = useRecoilValue(walletState);
  const address = wallet?.address;
  const { push, pathname } = useRouter();

  useEffect(() => {
    console.log(address);
    if (!address && pathname.indexOf('/hospital') === -1) push('/login');
  }, [address]);

  return (
    <div className="flex items-center h-20 w-full bg-gray-200 px-4">
      <p className="text-center text-primary text-s font-bold mr-auto">
        <span className="text-darkpurple">BCS</span>
        <span className="text-darkpurple text-s">(</span>
        <span className="text-grey">I</span>
        <span className="text-darkblue">0</span>
        <span className="text-darkblue">0</span>
        <span className="text-darkblue">0</span>
        <span className="text-neon">X</span>
      </p>
      <Address address={address} addressIndex={1} />
    </div>
  );
};

export default NavBar;
