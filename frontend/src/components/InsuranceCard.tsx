import React from "react";
import Link from "next/link";
import "tailwindcss/tailwind.css";
import Image from "next/image";

interface InsuranceCardProps {
  title: string;
  detail: string;
  linkTo: string;
}

export const InsuranceCard = ({
  title,
  detail,
  linkTo,
}: InsuranceCardProps) => {
  return (
    <Link href={linkTo}>
      <a>
        <div className="rounded-md flex-none w-32 h-44 mt-6 flex flex-col group shadow-lg bg-white cursor-pointer">
          <Image
            className="rounded-t-md"
            src="https://picsum.photos/300/200"
            alt="Picture of the author"
            width={128}
            height={80}
          />
          <div className="px-2 pt-1">
            <p className="font-medium text-sm">{title}</p>
            <p className="text-xs">{detail}</p>
          </div>
        </div>
      </a>
    </Link>
  );
};
