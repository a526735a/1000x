import React from 'react'
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';

interface IdCardDialogProps {
  open: boolean;
  inputRef: React.RefObject<HTMLInputElement>;
  handleClose: () => void;
  handleSubmit: () => void;
}

const IdCardDialog: React.FC<IdCardDialogProps> = ({ open, inputRef, handleClose, handleSubmit }) => {
  return (
    <Dialog onClose={handleClose} open={open} PaperProps={{ className: "px-10 py-4" }} >
      <DialogTitle>Card Number</DialogTitle>
      <input className="border w-full p-1 rounded-md shadow-md" ref={inputRef} type="password" />
      <button
        className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full mt-5"
        onClick={handleSubmit}
      >
        Submit
      </button>
    </Dialog>
  )
}

export default IdCardDialog
