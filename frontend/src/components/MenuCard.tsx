import React from 'react';
import Link from 'next/link';
import 'tailwindcss/tailwind.css';

interface MenuCardProps {
  title: string;
  detail: string;
  linkTo: string;
  icon: string;
}

export const MenuCard = ({ title, detail, linkTo, icon }: MenuCardProps) => {
  return (
    <Link href={linkTo}>
      <a>
        <div className="w-full h-30 p-6 mt-6 flex flex-row items-center group shadow-lg bg-white cursor-pointer">
          <img className="w-14" src={icon} alt="Glowing Crystal" />
          <div className="pl-4">
            <p className="text-primary text-xl font-bold group-hover:text-gray-900">
              {title}
            </p>
            <p className="text-primary group-hover:text-gray-500">{detail}</p>
          </div>
        </div>
      </a>
    </Link>
  );
};
