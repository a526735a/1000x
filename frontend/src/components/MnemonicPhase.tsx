import { FC, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSetRecoilState } from 'recoil';
import { createMnemonicPhase, getWalletFromMnemonic } from '~/address';
import { hashString, symmetricEncryptString } from '~/crypto';
import { passwordState } from '~/recoil/passwordState';
import { walletState } from '~/recoil/walletState'

interface MnemonicPhaseProps {
  password: string;
}

const MnemonicPhase: FC<MnemonicPhaseProps> = ({ password }) => {
  const generatedMnemonic = createMnemonicPhase();
  const setPassword = useSetRecoilState(passwordState)
  const setWallet = useSetRecoilState(walletState)
  const { push } = useRouter();

  const handleHome = () => {
    push('/');
  }

  useEffect(() => {
    if (window) {
      const encryptedMnemonic = symmetricEncryptString(password, generatedMnemonic);
      const salt = encryptedMnemonic.slice(0, 4);
      window.localStorage.setItem('mnemonic', encryptedMnemonic)
      const hash = hashString(`${password}${salt}`)
      console.log({ encryptedMnemonic, salt, hash });
      window.localStorage.setItem('password', hash);
    }
  }, [generatedMnemonic, password])

  useEffect(() => {
    setPassword(password);
    setWallet(getWalletFromMnemonic(generatedMnemonic));
  }, [generatedMnemonic, password, setPassword, setWallet]);

  return (
    <div className="flex flex-col gap-9">
    <p className="text-center text-primary text-5xl font-bold">
        <span className="text-darkpurple">BCS</span>
        <span className="text-darkpurple text-7xl">(</span>
        <span className="text-grey">I</span>
        <span className="text-darkblue">0</span>
        <span className="text-darkblue">0</span>
        <span className="text-darkblue">0</span>
        <span className="text-neon">X</span>
    </p>
    <span className="text-red-500 font-bold text-lg">Mnemonic Phase:</span>
    {generatedMnemonic}
    <span className="text-red-500 font-bold text-lg">Please do not share with others and remember to keep safe</span>
    <span className="text-red-500 font-bold text-lg">This is required to restore your digital wallet</span>
    <button
      className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      onClick={handleHome}
    >Home</button>
    </div>
  );
}

export default MnemonicPhase;
