import React from 'react';

import QRCode from 'react-qr-code';

interface InsuranceCardProps {
  value: string;
}

export const QrCard = ({ value }: InsuranceCardProps) => {
  return (
    <div className="flex flex-col h-96 w-full rounded-lg shadow-lg bg-white">
      <div className="flex justify-center items-center h-16 bg-primary w-full rounded-t-lg">
        <p className="text-white font-bold text-xl">QR CODE</p>
      </div>
      <div className="flex flex-col items-center justify-center flex-1">
        <QRCode size={120} value={value} />
        <p className="mt-4 text-primary font-medium">
          scan QR to claim insurance
        </p>
        <p className="text-sm">Name: Mr.1000x hackaton</p>
        <p className="text-sm">ID: 1100110010101</p>
        <p className="text-grey text-xs">ref-number 004488192847</p>
      </div>
      <div className="flex justify-center items-center h-16 border w-full rounded-b-lg">
        <p className="text-center text-primary text-xl font-bold">
          <span className="text-darkpurple">BCS</span>
          <span className="text-darkpurple text-2xl">(</span>
          <span className="text-grey">I</span>
          <span className="text-darkblue">0</span>
          <span className="text-darkblue">0</span>
          <span className="text-darkblue">0</span>
          <span className="text-neon">X</span>
        </p>
      </div>
    </div>
  );
};
