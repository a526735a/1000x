import React, { useState, useRef, useEffect } from 'react'
import { useSnackbar } from 'notistack';
import { useRecoilValue, useRecoilState } from 'recoil';
import { idCardState } from '~/recoil/idCardState';
import { passwordState } from '~/recoil/passwordState';
import { symmetricEncryptString, symmetricDecryptString } from '~/crypto';
import IdCardDialog from '~/components/IdCardDialog';
import { AiOutlineClose} from 'react-icons/ai';

const IdCardWarning: React.FC = () => {
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const enterIdRef= useRef<HTMLInputElement>(null);
  const { enqueueSnackbar } = useSnackbar();
  const password = useRecoilValue(passwordState);
  const [idCard, setIdCard] = useRecoilState(idCardState);
  const [isShow, setIsShow] = useState<boolean>(false);

  const handleSubmit = () => {
    if (typeof window === 'undefined') return null;
    const enteredId = enterIdRef?.current?.value;
    if (!enteredId) {
      enqueueSnackbar('Please Enter ID card number', { variant: 'error'});
    } else if (enteredId.length !== 13){
      enqueueSnackbar('Please Enter valid ID card number', { variant: 'error'});
    } else {
      setIdCard(enteredId);
      window.localStorage.setItem('id', symmetricEncryptString(password, enteredId));
      setOpenDialog(false);
    }
  }

  useEffect(() => {
    const encryptedId = window.localStorage.getItem('id');
    if (typeof window !== 'undefined' && password && encryptedId) {
      const id = symmetricDecryptString(password, encryptedId);
      setIdCard(id);
    } else if (typeof window !== 'undefined' && !encryptedId) {
      setIsShow(true);
    }
  }, [idCard]);

  if (idCard || !password || !isShow) return null;

  return (
    <div className=" bg-yellow-300 absolute mt-4 mx-2 rounded">
      <div className="relative px-2 py-2">
        <AiOutlineClose className="cursor-pointer absolute right-2 top-3" onClick={() => { setIsShow(false); }} />
        <p className="whitespace-normal">Please Provide ID card number to the digital wallet to enable all feature</p>
        <button
          onClick={() => { setOpenDialog(true); }}
          className="text-red-600"
        >
          Here
        </button>
        <IdCardDialog
          handleClose={() => { setOpenDialog(false); }}
          handleSubmit={handleSubmit}
          open={openDialog}
          inputRef={enterIdRef}
        />
      </div>
    </div>
  )
}

export default IdCardWarning;
