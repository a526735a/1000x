import pbkdf2 from 'pbkdf2';
import keccak from 'keccak';
import aes from 'aes-js';

export const getSeedByPbkdf2Sync = (key: string, salt: string) => {
  return pbkdf2.pbkdf2Sync(key, salt, 2048, 512)
}

export const getMasterKeyFromMnemonic = (mnemonic: string) => {
  const generatedSeeed = getSeedByPbkdf2Sync(mnemonic, 'mnemonicthousandX');
  const generatedKey = getSeedByPbkdf2Sync(generatedSeeed.toString('hex'), 'ETH seed')
  return generatedKey;
}

export const getMasterPrivateKeyFromMnemonic = (mnemonic: string) => {
  return getMasterKeyFromMnemonic(mnemonic);
}

export const hashString = (string: string) => {
  return keccak('sha3-256').update(string).digest().toString('hex');
}

export const symmetricEncryptString = (key: string, message: string) => {
  const paddedKey32Bytes = keccak('keccak256').update(key).digest();
  const messageBytes = aes.utils.utf8.toBytes(message);
  const aesCtr = new aes.ModeOfOperation.ctr(paddedKey32Bytes, new aes.Counter(5));
  const encryptedBytes = aesCtr.encrypt(messageBytes);
  const encryptedHex = aes.utils.hex.fromBytes(encryptedBytes);
  return encryptedHex;
}

export const symmetricDecryptString = (key: string, encryptedMessage: string) => {
  const paddedKey32Bytes = keccak('keccak256').update(key).digest();
  const encryptedBytes = aes.utils.hex.toBytes(encryptedMessage);
  const aesCtr = new aes.ModeOfOperation.ctr(paddedKey32Bytes, new aes.Counter(5));
  const decryptedBytes = aesCtr.decrypt(encryptedBytes);
  const decryptedText = aes.utils.utf8.fromBytes(decryptedBytes);
  return decryptedText;
}