
import randomBytes from 'randombytes';
import { ethers, Wallet } from 'ethers';
import { entropyToMnemonic } from 'bip39';
import { getInsuranceContractAddress } from '../../utils/addressHelpers';
import InsuranceABI from '../../abi/Insurance.json';

export const createMnemonicPhase = () => entropyToMnemonic(randomBytes(32));

export const getWalletFromMnemonic = (mnemonic: string, index?: number) => (
  ethers.Wallet.fromMnemonic(mnemonic, `m/44'/60'/0'/0/${index ?? 0}`)
)

export const getProvider = () => {
  const rpcUrl = process.env.RPC_URL;
  const chainId = process.env.CHAIN_ID;
  return new ethers.providers.JsonRpcProvider(
    rpcUrl,
    Number(chainId),
  );
}

export const getInsuranceContract = () => {
  const provider = getProvider();
  const insurancePrivateKey = process.env.INSURANCE_COMPANY_PRIVATE_KEY!;
  const walletPrivateKey = new Wallet(insurancePrivateKey);
  const wallet = walletPrivateKey.connect(provider);
  const insuranceAddress = getInsuranceContractAddress();
  return new ethers.Contract(
    insuranceAddress,
    InsuranceABI.abi,
    wallet,
  );
}

export const getInsuranceWallet = () => {
  const provider = getProvider();
  const insurancePrivateKey = process.env.INSURANCE_COMPANY_PRIVATE_KEY!;
  const walletPrivateKey = new Wallet(insurancePrivateKey);
  return walletPrivateKey.connect(provider);
}

export const getUserWalletFromPrivatekey = (privateKey: string) => {
  const provider = getProvider();
  const walletPrivateKey = new Wallet(privateKey);
  return walletPrivateKey.connect(provider);
}