import type { NextPage } from "next";
import { QrCard } from "~/components/QrCard";
import "tailwindcss/tailwind.css";

const InsuranceDetail: NextPage = () => {
  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <div className="flex flex-col items-center">
          <QrCard value="http://localhost:3000/hospital/customer" />
        </div>

        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">POLICY TITLE</p>
              <p className="text-primary">SUBTITLE</p>
            </div>
          </div>
        </div>
        <table className="shadow-lg mt-4">
          <tr className="bg-primary text-white">
            <th>ตัวอย่างผลประโยชน์</th>
            <th>แผน 1 ล้านบาท</th>
            <th>แผน 5 ล้านบาท</th>
          </tr>
          <tr>
            <td>Alfreds Futterkiste</td>
            <td>Maria Anders</td>
            <td>Germany</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
        </table>
      </div>
    </div>
  );
};

export default InsuranceDetail;
