import { useEffect, useState } from 'react';
import type { NextPage } from 'next';
import { InsuranceCard } from '~/components/InsuranceCard';
import { useRecoilValue } from 'recoil';
import { walletState } from '~/recoil/walletState';
import { ethers } from 'ethers';
import { CircularProgress } from '@mui/material';
import { getInsuranceContractAddress } from '../../../utils/addressHelpers';
import InsuranceABI from '../../../abi/Insurance.json';
import { getUserWalletFromPrivatekey } from '~/address';
import 'tailwindcss/tailwind.css';

const OwnedInsurance: NextPage = () => {
  const [ownedInsurance, setOwnedInsurance] = useState<number>(0);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const wallet = useRecoilValue(walletState);

  const fetchInsurance = async () => {
    if (!wallet) return null;

    const userWallet = getUserWalletFromPrivatekey(wallet.privateKey);
    const insuranceAddress = getInsuranceContractAddress();
    const signerInsurance = new ethers.Contract(
      insuranceAddress,
      InsuranceABI.abi,
      userWallet,
    );

    const result = await signerInsurance.searchInsurance(wallet.address);
    setOwnedInsurance(result.length);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchInsurance();
  });

  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <p className="text-primary font-bold text-xl">Owned Insurance</p>

        <div className="w-full h-32 p-6 mt-6 flex flex-col justify-center items-center group shadow-lg bg-white cursor-pointer">
          <p className="text-primary font-bold text-xl">
            Total Owned Insurance
          </p>
          {isLoading ? <CircularProgress /> : <p className="text-primary font-bold text-xl">{ownedInsurance}</p>}
        </div>
        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">Life Insurance</p>
              <p className="text-primary">your life insurance</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
          </div>
        </div>

        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">Accident Insurance</p>
              <p className="text-primary">your accident insurance</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default OwnedInsurance;
