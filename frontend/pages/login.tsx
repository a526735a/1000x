import { useEffect } from 'react';
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useRef } from 'react';
import { CgImport } from 'react-icons/cg';
import { useSetRecoilState, useRecoilState } from "recoil";
import { useSnackbar } from 'notistack';
import { hashString, symmetricDecryptString } from "~/crypto";
import { walletState } from "~/recoil/walletState";
import { passwordState } from "~/recoil/passwordState";
import { getWalletFromMnemonic } from "~/address";

const Login: NextPage = () => {
  const passwordRef = useRef<HTMLInputElement>(null);
  const { push } = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const setWallet = useSetRecoilState(walletState);
  const [password, setPassword] = useRecoilState(passwordState);

  useEffect(() => {
    console.log({ password });
    if (password) push('/');
  }, [password]);

  const handleLogin = () => {
    if (typeof window === 'undefined') return;
    const enteredPassword = passwordRef?.current?.value;
    const encryptedMnemonic =  window?.localStorage?.getItem('mnemonic');
    const passwordHash = window?.localStorage?.getItem('password');
    const salt = encryptedMnemonic?.slice(0, 4);
    if (enteredPassword && passwordHash) {
      const enteredPasswordHash = hashString(`${enteredPassword}${salt}`);
      if (enteredPasswordHash === passwordHash && encryptedMnemonic) {
        const walletIndex = window.localStorage.getItem('walletIndex');
        const mnemonic = symmetricDecryptString(enteredPassword, encryptedMnemonic);
        console.log({ enteredPasswordHash, mnemonic });
        setWallet(getWalletFromMnemonic(mnemonic, Number(walletIndex || '0')))
        setPassword(enteredPassword);
        push('/')
      } else enqueueSnackbar('Wrong Passwrod or No Account', { variant: 'error' });
    } else if (!enteredPassword) {
      enqueueSnackbar('Please Enter Passwrod', { variant: 'error' });
    } else {
      enqueueSnackbar('No Account', { variant: 'error' });
    }
  }

  const handleCreate = () => {
    push('/create-wallet');
  }

  const handleImport = () => {
    push('import-mneomic');
  }
  return (
    <div className="flex flex-col h-screen">
      <div className="flex h-full w-full shadow-lg">
        <div className="flex-1 px-8 pt-20 bg-white">
          <div className="flex flex-col gap-10">
            <div className="flex flex-col gap-4">
              <div>
                <p className="text-grey text-sm mb-2">
                  Password
                </p>
                <input className="border w-full p-1 rounded-md shadow-md" ref={passwordRef} type="password" />
              </div>
            </div>
            <button
              className="bg-transparent text-primary hover:text-blue-700 font-bold py-2 px-4 border border-blue-500 rounded"
              onClick={handleLogin}
            >
              Login
            </button>
            <button
              className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              onClick={handleCreate}
            >
              Create
            </button>
            {/* <button
              className="bg-yellow-700 hover:bg-yellow-600 text-white font-bold py-2 px-4 rounded flex justify-center items-center"
              onClick={handleImport}
            >
              <CgImport className="mr-2" />
              <p>Import</p>
            </button> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
