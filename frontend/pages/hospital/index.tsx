import type { NextPage } from "next";
import Link from "next/link";

const Hospital: NextPage = () => {
  return (
    <div className="flex flex-col h-screen">
      <div className="flex h-full w-full shadow-lg">
        <div className="flex-1 p-8 bg-white">
          <div className="flex flex-col gap-4">
            <p className="text-center text-primary text-5xl font-bold">
              <span className="text-darkpurple">HOSPITAL</span>
            </p>

            <Link href="/hospital/qr-scan">
              <a className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
                ScanQR
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hospital;
