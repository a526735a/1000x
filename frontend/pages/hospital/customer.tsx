import type { NextPage } from 'next';
import { InsuranceCard } from '~/components/InsuranceCard';
import 'tailwindcss/tailwind.css';

const Customer: NextPage = () => {
  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <p className="text-primary font-bold text-xl">CUSTOMER INFORMATION</p>

        <div className="w-full h-32 p-6 mt-6 flex flex-col justify-center items-center group shadow-lg bg-white cursor-pointer">
          <p className="text-primary font-bold text-xl">
            Total Owned Insurance
          </p>
          <p className="text-primary font-bold text-xl">48</p>
        </div>
        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">INSURANCE TITLE</p>
              <p className="text-primary">SUBTITLE</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
          </div>
        </div>

        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">INSURANCE TITLE</p>
              <p className="text-primary">SUBTITLE</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
            <InsuranceCard
              linkTo="/wallet/insurance/detail"
              title="TITLE"
              detail="Covid-19"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Customer;
