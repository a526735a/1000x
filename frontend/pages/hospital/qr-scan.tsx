import type { NextPage } from "next";
import QrReader from "react-qr-reader";
import Link from "next/link";
import { useRouter } from "next/router";

const HospitalQRScan: NextPage = () => {
  const router = useRouter();
  const handleScan = (data: any) => {
    if (data) {
      router.push(data);
    }
  };
  const handleError = (err: any) => {
    console.error(err);
  };

  return (
    <div className="flex flex-col h-screen">
      <div className="flex h-full w-full shadow-lg">
        <div className="flex-1 p-8 bg-white">
          <div className="flex flex-col gap-4">
            <p className="text-center text-primary text-5xl font-bold">
              <span className="text-darkpurple">HOSPITAL</span>
            </p>

            <QrReader
              delay={300}
              onError={handleError}
              onScan={handleScan}
              style={{ width: "100%" }}
            />

            <Link href="/hospital">
              <a className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
                Back
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HospitalQRScan;
