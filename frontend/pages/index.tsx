import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useRecoilValue } from 'recoil';
import 'tailwindcss/tailwind.css';
import { passwordState } from '~/recoil/passwordState';
import { MenuCard } from '~/components/MenuCard';

const Main: NextPage = () => {
  const { replace } = useRouter();
  const password = useRecoilValue(passwordState);

  if (!password) return null;
  return (
    <div className="flex flex-col h-screen">
      <div className="flex h-full w-full shadow-lg">
        <div className="flex-1 p-8 bg-secondary">
          <p className="text-primary text-xl font-bold">Menu</p>
          <MenuCard
            icon="/wallet-icon.png"
            linkTo="/wallet/insurance"
            title="Owned Insurance"
            detail="detail"
          />
          <MenuCard
            icon="/buy-icon.png"
            linkTo="/buy-insurance"
            title="Buy Insurance"
            detail="detail"
          />
        </div>
      </div>
    </div>
  );
};

export default Main;
