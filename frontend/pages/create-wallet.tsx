import type { NextPage } from 'next';
import { useRef, useState } from 'react';
import { useSnackbar } from 'notistack';
import CreatePassWord from '~/components/CreatePassWord';
import MnemonicPhase from '~/components/MnemonicPhase';

const CreateWallet: NextPage = () => {
  const newPasswordRef = useRef<HTMLInputElement>(null);
  const confirmPasswordRef = useRef<HTMLInputElement>(null);
  const [isConfirm, setIsConfirm] = useState<boolean>(false);
  const { enqueueSnackbar } = useSnackbar();

  const handleConfirm = () => {
    if (newPasswordRef.current && confirmPasswordRef.current) {
      const newPassword = newPasswordRef.current.value;
      const confirmPassword = newPasswordRef.current.value;
      if (newPassword?.length < 8) {
        enqueueSnackbar('Password need to be longer than 8 character', {
          variant: 'error',
        });
      } else if (newPassword === confirmPassword) {
        setIsConfirm(true);
      } else enqueueSnackbar('Please provide same password and confirm password', {
        variant: 'error',
      });
    }
  }

  return (
    <div className="flex flex-col h-screen">
      <div className="flex h-full w-full shadow-lg">
        <div className="flex-1 p-8 bg-white">
          {!isConfirm || !newPasswordRef?.current?.value ? 
            <CreatePassWord
              newPasswordRef={newPasswordRef}
              confirmPasswordRef={confirmPasswordRef}
              handleConfirm={handleConfirm}
            /> 
            : <MnemonicPhase password={newPasswordRef.current.value} />
          }
        </div>
      </div>
    </div>
  );
};

export default CreateWallet;
