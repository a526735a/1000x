import { NextApiRequest, NextApiResponse } from 'next'
import { getInsurance } from 'prisma/data';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const {
    method,
  } = req

  switch (method) {
    case 'GET':
      res.status(200)
      res.send(getInsurance());
      break
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}