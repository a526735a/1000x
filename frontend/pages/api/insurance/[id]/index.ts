import { NextApiRequest, NextApiResponse } from 'next'
import { getInsurance, setInsurance } from 'prisma/data';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const {
    query: { id },
    method,
    body: { policy, id: insuranceId, name, info },
  } = req

  switch (method) {
    case 'GET':
      res.status(200);
      res.send(getInsurance().find((ins) => ins.id === id));
      break
    case 'PATCH':
      res.status(200);
      setInsurance(getInsurance().map((ins) => ins.id === id ? ({ ...ins, policy }): ins));
      res.send(getInsurance())
      break
    case 'PUT':
      res.status(200);
      setInsurance([...getInsurance(), { name, info, id: insuranceId, policy }]);
      res.send(getInsurance())
    case 'DELETE':
      res.status(200);
      setInsurance(getInsurance().filter((ins) => ins.id !== id));
      res.send(getInsurance())
      break
    default:
      res.setHeader('Allow', ['GET', 'PATCH', 'DELETE'])
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}