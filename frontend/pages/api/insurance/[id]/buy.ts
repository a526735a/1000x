import { NextApiRequest, NextApiResponse } from 'next';
import { getInsuranceContract, getInsuranceWallet } from '~/address';

export default async function insuranceHandler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const {
    method,
    body,
  } = req;

  switch (method) {
    case 'POST':
      const userAddress = JSON.parse(body).address;
      const insuranceWallet = getInsuranceWallet();
      const insuranceContract = getInsuranceContract();

      const coverage = {
        OPD: { value: 1000, currency: 'baht', condition: '30 times per year' },
        IPD: { value: 2000, currency: 'baht', condition: '10 times per year' },
      };

      const newCard = {
        isInsuranceInfo: true,
        company: insuranceWallet.address,
        companyId: insuranceWallet.address,
        insuranceType: 'HEALTH',
        validFrom: 160082295,
        validTo: 170082295,
        coverage: JSON.stringify(coverage),
      };

      await insuranceContract.addInsuranceToUser(userAddress, newCard);

      res.status(200);
      break;
    default:
      res.setHeader('Allow', ['POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
