import "../styles/globals.css";
import type { AppProps } from 'next/app';
import { SnackbarProvider } from 'notistack';
import { RecoilRoot } from 'recoil';
import IdCardWarning from "~/components/IdCardWarning";
import NavBar from "~/components/NavBar";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <RecoilRoot>
      <SnackbarProvider
        maxSnack={3}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        preventDuplicate
      >
        <div className="flex justify-center bg-black relative">
          <div className="w-96 relative">
            <IdCardWarning />
            <NavBar />
            <Component {...pageProps} />
          </div>
        </div>
      </SnackbarProvider>
    </RecoilRoot>
  );
}

export default MyApp;
