import type { NextPage } from 'next';
import 'tailwindcss/tailwind.css';

const RegisterInsurance: NextPage = () => {
  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <p className="text-primary font-bold text-xl">APPLY INSURANCE</p>
        <div className="flex flex-col gap-4">
          <div className="mt-6">
            <div className="flex items-center">
              <div className="bg-primary w-4 h-10 mr-2" />
              <div>
                <p className="text-primary font-bold">USER INFORMATION</p>
                <p className="text-primary">SUBTITLE</p>
              </div>
            </div>
          </div>
          <div>
            <p className="text-grey text-sm mb-2">Name</p>
            <input
              className="border w-full p-1 rounded-md shadow-md"
              type="text"
            />
          </div>
          <div>
            <p className="text-grey text-sm mb-2">Surname</p>
            <input
              className="border w-full p-1 rounded-md shadow-md"
              type="text"
            />
          </div>
          <div>
            <p className="text-grey text-sm mb-2">Telephone number</p>
            <input
              className="border w-full p-1 rounded-md shadow-md"
              type="text"
            />
          </div>

          <div>
            <p className="text-grey text-sm mb-2">ID card number</p>
            <input
              className="border w-full p-1 rounded-md shadow-md"
              type="text"
            />
          </div>

          <div>
            <p className="text-grey text-sm mb-2">Address</p>
            <input
              className="h-24 border w-full p-1 rounded-md shadow-md"
              type="text"
            />
          </div>

          <button className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default RegisterInsurance;
