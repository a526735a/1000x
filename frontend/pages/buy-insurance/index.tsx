import type { NextPage } from 'next';
import { InsuranceCard } from '~/components/InsuranceCard';
import 'tailwindcss/tailwind.css';
import useSWR from 'swr'
import { fetcher } from '~/api';
import { Insurance } from '~/type';

const BuyInsurance: NextPage = () => {

  const { data: insurances } = useSWR<Insurance[]>(
    '/api/insurance',
    fetcher
  )

  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <p className="text-primary font-bold text-xl">INSURANCE LIST</p>

        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">Recommended</p>
              <p className="text-primary">Recommend insurance for your age</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
            {insurances?.map(({ name, info, id }) => (
              <InsuranceCard
                linkTo={`/buy-insurance/${id}`}
                title={name}
                detail={info}
                key={`recommend-${id}`}
              />
            ))}
          </div>
        </div>

        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">Popular Insurances</p>
              <p className="text-primary">Most buy insurances in the past month</p>
            </div>
          </div>
          <div className="flex gap-4 overflow-x-auto">
          {insurances?.map(({ name, info, id }) => (
            <InsuranceCard
              linkTo={`/buy-insurance/${id}`}
              title={name}
              detail={info}
              key={`popular-${id}`}
            />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default BuyInsurance;
