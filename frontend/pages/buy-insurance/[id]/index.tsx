import React from 'react';
import { useRouter } from 'next/router'
import "tailwindcss/tailwind.css";
import useSWR from 'swr'
import { fetcher } from '~/api';
import { Insurance } from "~/type";

const InsuranceDetail: React.FC = () => {
  const { query, push } = useRouter()

  const { data: insurance } = useSWR<Insurance>(
    `/api/insurance/${query.id}`,
    fetcher,
  );

  const handleBuy = () => {
    push(`/buy-insurance/${query.id}/ndid`)
  }

  if (!insurance) return null;

  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8 flex flex-col">
        <div className="mt-6">
          <div className="flex flex-row items-center">
            <p className="text-primary font-bold">Insurance Name</p>
            <p className="text-primary ml-auto">{insurance.name}</p>
          </div>
        </div>
        <div className="mt-6">
          <div className="flex flex-row items-center">
            <p className="text-primary font-bold">Insurance Info</p>
            <p className="text-primary ml-auto">{insurance.info}</p>
          </div>
        </div>
        <div className="mt-6">
          <div className="flex items-center">
            <div className="bg-primary w-4 h-10 mr-2" />
            <div>
              <p className="text-primary font-bold">POLICY TITLE</p>
              <p className="text-primary">SUBTITLE</p>
            </div>
          </div>
          <p className="mt-5">{insurance.policy}</p>
        </div>
        <table className="shadow-lg mt-4">
          <tr className="bg-primary text-white">
            <th>ตัวอย่างผลประโยชน์</th>
            <th>แผน 1 ล้านบาท</th>
            <th>แผน 5 ล้านบาท</th>
          </tr>
          <tr>
            <td>Alfreds Futterkiste</td>
            <td>Maria Anders</td>
            <td>Germany</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
        </table>
        <button
          className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-7/12 mt-10 self-center"
          onClick={handleBuy}
        >
          Buy
        </button>
      </div>
    </div>
  );
};

export default InsuranceDetail;
