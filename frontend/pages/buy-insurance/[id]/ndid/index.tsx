import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { BankCard } from '~/components/BankCard';
import 'tailwindcss/tailwind.css';

const VerifyNDID: NextPage = () => {
  const { query } = useRouter();

  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <p className="text-primary font-bold text-xl">VERIFY NDID</p>
        <div className="flex flex-col gap-4">
          <div className="mt-6">
            <div className="flex items-center">
              <div className="bg-primary w-4 h-10 mr-2" />
              <div>
                <p className="text-primary font-bold">BANK LIST</p>
                <p className="text-primary">SUBTITLE</p>
              </div>
            </div>
          </div>
          <BankCard
            title="KBANK"
            icon="/kplus-icon.png"
            linkTo={`/buy-insurance/${query.id}/ndid/status`}
          />
          <BankCard
            title="SCB"
            icon="/scb-icon.png"
            linkTo={`/buy-insurance/${query.id}/ndid/status`}
          />
          <BankCard
            title="KRUNGSRI"
            icon="/kma-icon.png"
            linkTo={`/buy-insurance/${query.id}/ndid/status`}
          />
          <BankCard
            title="TMB"
            icon="/tmb-icon.jpg"
            linkTo={`/buy-insurance/${query.id}/ndid/status`}
          />
        </div>
      </div>
    </div>
  );
};

export default VerifyNDID;
