import { useState } from 'react';
import type { NextPage } from 'next';
import Countdown from 'react-countdown';
import 'tailwindcss/tailwind.css';
import { useRecoilValue } from 'recoil';
import { walletState } from '~/recoil/walletState';
import { useRouter } from 'next/router';

const RegisterInsurance: NextPage = () => {
  const { push, query } = useRouter();
  const [status, setStatus] = useState('Pending');
  const wallet = useRecoilValue(walletState);

  const renderer = ({ hours, minutes, seconds, completed }: any) => {
    if (completed && wallet) {
      setStatus('Complete');
      fetch(`/api/insurance/${query.id}/buy`, { method: 'POST', body: JSON.stringify({ address: wallet.address }) })
    }
    return (
      <span className="text-primary font-bold mb-4">
        00:00:0{seconds}
      </span>
    );
  };

  const handleBackBtn = () => {
    push('/');
  };

  return (
    <div className="flex flex-col min-h-screen bg-secondary">
      <div className="flex-1 p-8">
        <div className="flex flex-col gap-4">
          <div className="flex flex-col items-center gap-4">
            <img className="w-24" src="/kplus-icon.png" alt="Kbank-icon" />
            <p className="text-primary font-bold">Verify Status: {status}</p>
            {status === 'Complete' ? (
              <button
                className="bg-primary hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                onClick={handleBackBtn}
              >
                Home
              </button>
            ) : (
              <>
                <p className="text-primary">
                  Go to verify ndid via bank account
                </p>
                <Countdown date={Date.now() + 5000} renderer={renderer} />
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterInsurance;
