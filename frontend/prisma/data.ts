interface Insurances {
  name: string;
  info: string;
  id: string;
  policy: string;
}

let insurances: Insurances[] = [
  { name: 'ประกันนะครับผม 1', info: 'ประกันนะครับผม 1 infomation', id: 'insurance-company-insurance-1', policy: 'โพลิซี' },
  { name: 'ประกันนะครับผม 2', info: 'ประกันนะครับผม 2 infomation', id: 'insurance-company-insurance-2', policy: 'โพลิซี' },
  { name: 'ประกันนะครับผม 3', info: 'ประกันนะครับผม 3 infomation', id: 'insurance-company-insurance-3', policy: 'โพลิซี' },
  { name: 'ประกันนะครับผม 4', info: 'ประกันนะครับผม 4 infomation', id: 'insurance-company-insurance-4', policy: 'โพลิซี' },
  { name: 'ประกันนะครับผม 5', info: 'ประกันนะครับผม 5 infomation', id: 'insurance-company-insurance-5', policy: 'โพลิซี' },
];

export const getInsurance = () => insurances;

export const setInsurance = (newInsurance: Insurances[]) => insurances = newInsurance;
