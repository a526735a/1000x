const path = require('path');
const withImages = require('next-images');

module.exports = withImages({
  images: {
    domains: ['picsum.photos', 'example2.com']
  },
  webpack(config) {
    // eslint-disable-next-line no-param-reassign
    config.resolve.alias['~'] = path.resolve(__dirname);
    return config;
  },
  env: {
    REACT_APP_CHAIN_ID: process.env.REACT_APP_CHAIN_ID ?? '4',
    CHAIN_ID: process.env.CHAIN_ID ?? '4',
    DATABASE_URL: process.env.DATABASE_URL ?? "postgresql://postgres@localhost:5432/thousandx",
    INSURANCE_COMPANY_PRIVATE_KEY: process.env.INSURANCE_COMPANY_PRIVATE_KEY ?? '0x84165e579d173b9d1d9239e2bee235a63ca87293eba7f0da9164e8ed825d2d29',
    RPC_URL: process.env.RPC_URL ?? 'https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161',
  },
});
