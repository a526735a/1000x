import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { BigNumberish } from "ethers";
import { ethers } from "hardhat";
import { Insurance } from "../typechain/Insurance";

type InsuranceInfo = {
  isInsuranceInfo: boolean;
  company: string;
  companyId: string;
  insuranceType: string;
  validFrom: BigNumberish;
  validTo: BigNumberish;
  coverage: string;
};
describe("Insurance", function () {
  let insurance: Insurance;
  let insuranceAccount: SignerWithAddress;
  let hospitalAccount: SignerWithAddress;
  let userAccount: SignerWithAddress;

  it("Should add insurance successfully", async function () {
    const accounts = await ethers.getSigners();
    insuranceAccount = accounts[0];
    hospitalAccount = accounts[1];
    userAccount = accounts[2];
    const Insurance = await ethers.getContractFactory("Insurance");
    insurance = await Insurance.deploy(
      [hospitalAccount.address],
      [insuranceAccount.address]
    );

    await insurance.deployed();

    // add insurance
    const newInsurance: InsuranceInfo = {
      isInsuranceInfo: true,
      company: "1000X",
      companyId: "1",
      insuranceType: "Health",
      validFrom: new Date().toUTCString(),
      validTo: new Date().toUTCString(),
      coverage: "OPD, IPD",
    };
    console.log(await insurance.addInsurance(newInsurance));

    const tx = await insurance.addInsuranceToUser(
      userAccount.address,
      newInsurance
    );

    // wait until the transaction is mined
    await tx.wait();

    // expect(await insurance.greet()).to.equal("Hola, mundo!");
  });
});
