// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

contract Insurance {
    struct InsuranceInfo {
        bool isInsuranceInfo;
        address company;
        address companyId;
        string insuranceType;
        uint256 validFrom;
        uint256 validTo;
        string coverage;
    }

    address[] public insuranceCompanies;
    address[] public hospitals;

    mapping(address => bool) isInsuranceCompany;
    mapping(address => bool) isHospital;

    mapping(address => InsuranceInfo[]) private userToInsurances;
    mapping(uint256 => InsuranceInfo) insurances;

    modifier onlyInsuranceCompany() {
        require(isInsuranceCompany[msg.sender], "must be insurance company");
        _;
    }
    modifier onlyHospital() {
        require(isHospital[msg.sender], "must be hospital");
        _;
    }
    modifier onlyUser(address user) {
        require(user == msg.sender, "must be user");
        _;
    }

    constructor(
        address[] memory _hospitals,
        address[] memory _insuranceCompanies
    ) {
        for (uint256 i = 0; i < _hospitals.length; i++) {
            isHospital[_hospitals[i]] = true;
            hospitals.push(_hospitals[i]);
        }
        for (uint256 i = 0; i < _insuranceCompanies.length; i++) {
            isInsuranceCompany[_insuranceCompanies[i]] = true;
            insuranceCompanies.push(_insuranceCompanies[i]);
        }
    }

    function _searchInsurance(address _user)
        internal
        view
        returns (InsuranceInfo[] memory)
    {
        return userToInsurances[_user];
    }

    function searchInsurance(address _user)
        public
        view
        onlyUser(_user)
        returns (InsuranceInfo[] memory)
    {
        return userToInsurances[_user];
    }

    function addInsuranceToUser(
        address _user,
        InsuranceInfo calldata _insuranceInfo
    ) public onlyInsuranceCompany {
        userToInsurances[_user].push(_insuranceInfo);
    }

    modifier mustSignWithECDSA(
        bytes32 h,
        address user,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) {
        require(ecrecover(h, v, r, s) == user);
        _;
    }

    function permitToSearchInsurance(
        bytes32 hash,
        address user,
        uint8 v,
        bytes32 r,
        bytes32 s
    )
        public
        view
        mustSignWithECDSA(hash, user, v, r, s)
        returns (InsuranceInfo[] memory)
    {
        return _searchInsurance(user);
    }
}
